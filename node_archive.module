<?php

/**
 * @file
 * 
 * This module provides a new node atate called "archived" which replaces deletions
 * It uses node_access to allow/disallow access to archived nodes based on 
 * permission: view archived nodes
 * 
 * Permission: restore archived nodes is used to allow roles to "restore" nodes -
 * essentially removing them from the archives state back into normallness
 * 
 * We have arranged the module into the following sections
 * 
 * 1. General helper functions
 * 2. Overrides to the admin/content/node page so we can filter on archived nodes
 * 3. Regular Drupal API functions
 * 4. Our own custom callbacks
 *
 */

/*****************************************************************
 * 
 * 1. Helper Functions
 * 
 *****************************************************************/

/**
 * Helper function returns 1 if node is archived, 0 if not
 */
function _node_archive_is_archived($nid) {
  return db_result(db_query("SELECT COUNT('nid') AS num FROM {node_archive} WHERE nid = %d", $nid));
}

/**
 * Helper function that processes menu_links on archive/restore
 * 
 * @param $op
 *   Either 'archive' or 'restore'
 * @param $menu_items
 *   An array of menu items as follows:
 *   $menu_items = array(
 *     <mlid> => <option>,
 *   );
 * 
 */
function _node_archive_menu_items($op, $menu_items) {
  // Settings for reference
  // Archive:
  // 0. Ignore
  // 1. Delete menu items
  // 2. Hide menu items
  // 
  // Restore:
  // 0. Ignore
  // 1. Enable hidden menu items
  
  $clear_cache = FALSE;
  
  switch ($op) {
    case 'archive':
      foreach ($menu_items as $mlid => $option) {
        if ($option == 1) {
          menu_link_delete($mlid);
        }
        elseif ($option == 2) {
          db_query("UPDATE {menu_links} SET hidden = '-1' WHERE mlid = %d", $mlid);
          $clear_cache = TRUE;
        }
      }
      break;
      
    case 'restore':
      foreach ($menu_items as $mlid => $option) {
        if ($option == 1) {
          db_query("UPDATE {menu_links} SET hidden = 0 WHERE mlid = %d", $mlid);
          $clear_cache = TRUE;
        }
      }
      break;
  }
  
  if ($clear_cache) {
    menu_cache_clear_all();
    _menu_clear_page_cache();
  }
  
}

/*****************************************************************
 * 
 * 1a. Helper/Callback functions that do the archiving/deleting
 * 
 *****************************************************************/

/**
 * Restore a group of nodes
 * 
 * @param $nids
 *   An array of NIDs
 * 
 * @param $mass
 *   Is this function being called by hook_node_operations()? If so $mass = TRUE
 *   It could also be called from node/%/restore
 *     If so we are dealing with a single NID and may need to alter menu items
 * 
 * @param $publish
 *   Publish the node
 * 
 * @param $menu_items
 *   An array of mlid as keys and their options as values
 *   Options:
 *     0. Ignore the menu item.
 *     1. Un-Hide the menu item.
 * 
 * Callback of hook_node_operations()
 * Also called from the confirm form at node/%/restore
 * 
 */
function node_archive_mass_restore($nids, $mass = TRUE, $publish = FALSE, $menu_items = array()) {
  foreach($nids as $nid) {
    $node = node_load($nid);
    if (!$node->node_archived) {
      drupal_set_message(t('Node %node is not !archived.',
        array(
          '%node' => $node->title, 
          '!archived' => strtolower(variable_get('node_archive_archived_syntax', 'Archived')),
        )
      ));
      return;
    }
    else {
      // let other modules know we are restoring this node
      module_invoke_all('node_archive', 'restore', $node);
      
      // deal with any disabled menu items (only for mass operations)
      if (variable_get('node_archive_menu_link_restore', '1') && $mass) {
        // check $mass to make sure we were called from hook_node_operations()
        $result = db_query("SELECT mlid FROM {menu_links}
                            WHERE link_path = 'node/%d' 
                            AND module = 'menu' 
                            AND hidden = '-1'", $node->nid);

        $menu_items = array();
        while ($row = db_fetch_array($result)) {
          $menu_items[$row['mlid']] = variable_get('node_archive_menu_link_restore', '1');
        }
        
        _node_archive_menu_items('restore', $menu_items);
      }
      
      if ($publish) {
        $node->status = 1;
        node_save($node);
      }
      
      // This will make the node "real" again!
      db_query("DELETE FROM {node_archive} WHERE nid = %d", $nid);
      
      // refresh this node's status in the node_access table
      node_access_acquire_grants($node);
      
      drupal_set_message(t('Node %node has been !restored.',
        array(
          '%node' => $node->title, 
          '!restored' => strtolower(variable_get('node_archive_restored_syntax', 'Restored')),
        )
      ));
    }
  }
  
  // not ideal we are doing this twice in the function,
  // once for a mass of nodes and once for a single node
  // This is a result of deciding to put menu options on every node but a 
  // single option for mass operations
  if (!$mass && count($nids) == 1 && count($menu_items)) {
    // The function has been called by the node/%/restore page.
    // There is a single node to deal with
    // AND there is something in $menu_items we need to look at
    _node_archive_menu_items('restore', $menu_items);
  }
}

/**
 * Archive a group of nodes
 * 
 * @param $nids
 *   An array of NIDs
 * 
 * @param $mass
 *   Is this function being called by hook_node_operations()? If so $mass = TRUE
 *   It could also be called from node/%/delete
 *     If so we are dealing with a single NID and may need to alter menu items
 * 
 * @param $menu_items
 *   An array of mlid as keys and their options as values
 *   Options:
 *     0. Ignore the menu item.
 *     1. Delete the menu item.
 *     2. Hide the menu item.
 * 
 * Callback of hook_node_operations()
 * Also called from the confirm form at node/%/delete
 * 
 */
function node_archive_mass_archive($nids, $mass = TRUE, $menu_items = array(), $delete_alias = FALSE) {
  global $user;
  
  foreach($nids as $nid) {
    $node = node_load($nid);
    if ($node->node_archived) {
      drupal_set_message(t('Node %node is already !archived.',
        array(
          '%node' => $node->title,
          '!archived' => strtolower(variable_get('node_archive_archived_syntax', 'Archived')),
        )
      ));
      return;
    }
    else {
      // always unpublish on archive
      $node->status = 0;
      node_save($node);
      
      // let other modules know we are archiving this node
      module_invoke_all('node_archive', 'archive', $node);
      
      // Deal with menu items
      // we do this with _node_archive_menu_items() which 
      // expects an $op and an array of mlid's 
      if (variable_get('node_archive_menu_link_archive', '2') && $mass) {
        // check $mass to make sure we were called from hook_node_operations()
        $result = db_query("SELECT mlid FROM {menu_links} 
                            WHERE link_path = 'node/%d' 
                            AND module = 'menu'", $node->nid);
        $menu_items = array();
        while ($row = db_fetch_array($result)) {
          $menu_items[$row['mlid']] = variable_get('node_archive_menu_link_archive', '2');
        }
        _node_archive_menu_items('archive', $menu_items);
      }
      
      // delete URL_alias if option set
      if (variable_get('node_archive_path_alias_archive', '1') && $mass) {
        // check $mass to make sure we were called from hook_node_operations()
        // delete the alias
        path_set_alias('node/'. $node->nid);
      }
      
      // sort of the most important thing ;)
      db_query("INSERT INTO {node_archive} (nid, uid, timestamp)
                VALUES (%d, %d, %d)", $nid, $user->uid, $_SERVER['REQUEST_TIME']);
      
      // refresh this node's status in the node_access table
      node_access_acquire_grants($node);
      
      drupal_set_message(t('Node %node has been !archived.',
        array(
          '%node' => $node->title,
          '!archived' => strtolower(variable_get('node_archive_archived_syntax', 'Archived')),
        )
      ));
    }
  }
  
  // not ideal we are doing some things twice in the function,
  // once for a mass of nodes and once for a single node
  // This is a result of deciding to put menu options on every node but a 
  // single option for mass operations
  if (!$mass && count($nids) == 1 && count($menu_items)) {
    // The function has been called by the node/%/delete page.
    // There is a single node to deal with
    // AND there is something in $menu_items we need to look at
    _node_archive_menu_items('archive', $menu_items);
  }
  if (!$mass && count($nids) == 1 && $delete_alias) {
    // delete the alias
    path_set_alias('node/'. $node->nid);
  }
}

/*****************************************************************
 * 
 * 2. Form/Callback overrides that modifies admin/content/node
 * to include our own filters and display of archived nodes
 * 
 *****************************************************************/

/**
 * Menu callback: content administration.
 * 
 * This is an override of node_admin_content() found in node.admin.inc
 * We override to provide our own functions:
 * -node_archive_filter_form() and node_archive_admin_nodes()
 * 
 */
function node_archive_admin_content($form_state) {
  if (isset($form_state['values']['operation']) && $form_state['values']['operation'] == 'archive') {
    return node_archive_multiple_archive_confirm($form_state, array_filter($form_state['values']['nodes']));
  }
  $form = node_archive_filter_form();

  $form['#theme'] = 'node_filter_form';
  $form['admin']  = node_archive_admin_nodes();

  return $form;
}

/**
 * Form builder: Builds the node administration overview.
 * 
 * This function is a replacement for node_admin_nodes()
 * 
 * We need our own function for node_build_filter_query() that provides filters for 
 * whether a node is archived or not... No hooks/alter for this :(
 * 
 * Also we edit the pager_query to join our node_archive table
 * 
 */
function node_archive_admin_nodes() {
  
  $filter = node_archive_build_filter_query();

  $result = pager_query(db_rewrite_sql(
    'SELECT n.*, u.name, na.nid as node_archived 
     FROM {node} n ' . $filter['join'] . ' 
     INNER JOIN {users} u ON n.uid = u.uid 
     LEFT JOIN {node_archive} na ON n.nid = na.nid ' . $filter['where'] . '
     ORDER BY n.changed DESC'), 50, 0, NULL, $filter['args']);

  // Enable language column if locale is enabled or if we have any node with language
  $count = db_result(db_query("SELECT COUNT(*) FROM {node} n WHERE language != ''"));
  $multilanguage = (module_exists('locale') || $count);

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );
  $options = array();
  foreach (module_invoke_all('node_operations') as $operation => $array) {
    $options[$operation] = $array['label'];
  }
  
  // we have added two operations for archiving a node
  // one without a callback so as to provide a confirm form for admin/content/node
  // one has a callback so it works with VBO. unset that one here as we don't need two!
  unset($options['archive-vbo']);
  
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => 'approve',
  );
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#submit' => array('node_admin_nodes_submit'),
  );

  $languages = language_list();
  $destination = drupal_get_destination();
  $nodes = array();
  while ($node = db_fetch_object($result)) {
    $nodes[$node->nid] = '';
    $options = empty($node->language) ? array() : array('language' => $languages[$node->language]);
    $form['title'][$node->nid] = array('#value' => l($node->title, 'node/' . $node->nid, $options) . ' ' . theme('mark', node_mark($node->nid, $node->changed)));
    $form['name'][$node->nid] =  array('#value' => check_plain(node_get_types('name', $node)));
    $form['username'][$node->nid] = array('#value' => theme('username', $node));
    
    if ($node->node_archived) {
      $form['status'][$node->nid] = array(
        '#value' => strtolower(variable_get('node_archive_archived_syntax', 'Archived'))
      );
    }
    else {
      $form['status'][$node->nid] = array('#value' => ($node->status ? t('published') : t('not published')));
    }
    
    if ($multilanguage) {
      $form['language'][$node->nid] = array('#value' => empty($node->language) ? t('Language neutral') : t($languages[$node->language]->name));
    }
    $form['operations'][$node->nid] = array('#value' => l(t('edit'), 'node/' . $node->nid . '/edit', array('query' => $destination)));
  }
  $form['nodes'] = array('#type' => 'checkboxes', '#options' => $nodes);
  $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));
  $form['#theme'] = 'node_admin_nodes';
  return $form;
}

/**
 * Build query for node administration filters based on session.
 * 
 * A replacement for node_build_filter_query() so we can provide a
 * filter for archived nodes.
 * 
 * 1. replace node_filters() with node_archive_filters()
 * 2. add the "na.nid IS NOT NULL" bit to alter the filter query
 * 
 */
function node_archive_build_filter_query() {
  $filters = node_archive_filters();

  // Build query
  $where = $args = array();
  $join = '';
  foreach ($_SESSION['node_overview_filter'] as $index => $filter) {
    list($key, $value) = $filter;
    switch ($key) {
      case 'status':
        // Note: no exploitable hole as $key/$value have already been checked when submitted
        list($key, $value) = explode('-', $value, 2);
        // this is a unique bit to node_archive module
        if ($key == 'archived') {
          $where[] = 'na.nid IS NOT NULL';
        }
        else {
          $where[] = 'n.' . $key . ' = %d';
          $where[] = 'na.nid IS NULL';
        }
        break;
        
      case 'category':
        $table = "tn$index";
        $where[] = "$table.tid = %d";
        $join .= "INNER JOIN {term_node} $table ON n.nid = $table.nid ";
        break;
      
      case 'type':
        $where[] = "n.type = '%s'";
        break;
      
      case 'language':
        $where[] = "n.language = '%s'";
        break;
    }
    $args[] = $value;
  }
  $where = count($where) ? 'WHERE ' . implode(' AND ', $where) : '';

  return array('where' => $where, 'join' => $join, 'args' => $args);
}

/**
 * Submit callback for 'node_delete_confirm' which we altered in hook_form_alter()
 * 
 * We are piggybacking on the node/%/delete menu callback
 * so permissions to delete and archive are the same
 * 
 */
function node_archive_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    node_archive_mass_archive(
      array($form_state['values']['nid']),
      FALSE, 
      $form_state['values']['menu_items'], 
      $form_state['values']['delete_alias']
    );
  }

  $form_state['redirect'] = '<front>';
}

/**
 * Return form for node administration filters.
 * 
 * This is the main form constructor for the filter form admin/content/node
 * We replace node_filter_form() to supply our own filters in node_archive_filters()
 * as well our submit function - node_archive_filter_form_submit() 
 * which does that actual operations
 * 
 */
function node_archive_filter_form() {
  $session = &$_SESSION['node_overview_filter'];
  $session = is_array($session) ? $session : array();
  $filters = node_archive_filters();

  $i = 0;
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Show only items where'),
    '#theme' => 'node_filters',
  );
  $form['#submit'][] = 'node_archive_filter_form_submit';
  foreach ($session as $filter) {
    list($type, $value) = $filter;
    if ($type == 'category') {
      // Load term name from DB rather than search and parse options array.
      $value = module_invoke('taxonomy', 'get_term', $value);
      $value = $value->name;
    }
    else if ($type == 'language') {
      $value = empty($value) ? t('Language neutral') : module_invoke('locale', 'language_name', $value);
    }
    else {
      $value = $filters[$type]['options'][$value];
    }
    if ($i++) {
      $form['filters']['current'][] = array('#value' => t('<em>and</em> where <strong>%a</strong> is <strong>%b</strong>', array('%a' => $filters[$type]['title'], '%b' => $value)));
    }
    else {
      $form['filters']['current'][] = array('#value' => t('<strong>%a</strong> is <strong>%b</strong>', array('%a' => $filters[$type]['title'], '%b' => $value)));
    }
    if (in_array($type, array('type', 'language'))) {
      // Remove the option if it is already being filtered on.
      unset($filters[$type]);
    }
  }

  foreach ($filters as $key => $filter) {
    $names[$key] = $filter['title'];
    $form['filters']['status'][$key] = array('#type' => 'select', '#options' => $filter['options']);
  }

  $form['filters']['filter'] = array('#type' => 'radios', '#options' => $names, '#default_value' => 'status');
  $form['filters']['buttons']['submit'] = array('#type' => 'submit', '#value' => (count($session) ? t('Refine') : t('Filter')));
  if (count($session)) {
    $form['filters']['buttons']['undo'] = array('#type' => 'submit', '#value' => t('Undo'));
    $form['filters']['buttons']['reset'] = array('#type' => 'submit', '#value' => t('Reset'));
  }

  drupal_add_js('misc/form.js', 'core');

  return $form;
}


/**
 * Process result from node administration filter form.
 * 
 * A shame we had to override the function node_filter_form_submit()
 * but we had to get our filters in here with node_archive_filters()
 * 
 */
function node_archive_filter_form_submit($form, &$form_state) {
  $filters = node_archive_filters();
  
  switch ($form_state['values']['op']) {
    case t('Filter'):
    case t('Refine'):
      if (isset($form_state['values']['filter'])) {
        $filter = $form_state['values']['filter'];

        // Flatten the options array to accommodate hierarchical/nested options.
        $flat_options = form_options_flatten($filters[$filter]['options']);

        if (isset($flat_options[$form_state['values'][$filter]])) {
          $_SESSION['node_overview_filter'][] = array($filter, $form_state['values'][$filter]);
        }
      }
      break;
      
    case t('Undo'):
      array_pop($_SESSION['node_overview_filter']);
      break;
    
    case t('Reset'):
      $_SESSION['node_overview_filter'] = array();
      break;
  }
}


/**
 * List node administration filters that can be applied.
 * 
 * A replacement for node_filters() so we can add in the archived filter
 * 
 */
function node_archive_filters() {
  // Regular filters
  $filters['status'] = array(
    'title' => t('status'),
    'options' => array(
      'status-1' => t('published'),
      'status-0' => t('not published'),
      'archived-0' => strtolower(variable_get('node_archive_archived_syntax', 'Archived')),
      'promote-1' => t('promoted'),
      'promote-0' => t('not promoted'),
      'sticky-1' => t('sticky'),
      'sticky-0' => t('not sticky'),
    ),
  );
  // Include translation states if we have this module enabled
  if (module_exists('translation')) {
    $filters['status']['options'] += array(
      'translate-0' => t('Up to date translation'),
      'translate-1' => t('Outdated translation'),
    );
  }

  $filters['type'] = array('title' => t('type'), 'options' => node_get_types('names'));

  // The taxonomy filter
  if ($taxonomy = module_invoke('taxonomy', 'form_all', 1)) {
    $filters['category'] = array('title' => t('category'), 'options' => $taxonomy);
  }
  // Language filter if there is a list of languages
  if ($languages = module_invoke('locale', 'language_list')) {
    $languages = array('' => t('Language neutral')) + $languages;
    $filters['language'] = array('title' => t('language'), 'options' => $languages);
  }
  return $filters;
}

/**
 * Callback form for when the archive operation has been executed.
 * 
 * This triggers a confirmation form.
 * 
 * Replacement/Alteration of node_multiple_delete_confirm() 
 * 
 */
function node_archive_multiple_archive_confirm(&$form_state, $nodes) {
  $form['nodes'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);
  // array_filter returns only elements with TRUE values
  foreach ($nodes as $nid => $value) {
    $title = db_result(db_query('SELECT title FROM {node} WHERE nid = %d', $nid));
    $form['nodes'][$nid] = array(
      '#type' => 'hidden',
      '#value' => $nid,
      '#prefix' => '<li>',
      '#suffix' => check_plain($title) . "</li>\n",
    );
  }
  $form['operation'] = array('#type' => 'hidden', '#value' => 'archive');
  $form['#submit'][] = 'node_archive_multiple_archive_confirm_submit';
  return confirm_form($form,
    t('Are you sure you want to !archive these items?',
      array('!archive' => strtolower(variable_get('node_archive_archive_syntax', 'Archive')))),
    'admin/content/node', t('This action <strong>can</strong> be undone.'),
    t('!archive all',
      array('!archive' => ucwords(variable_get('node_archive_archive_syntax', 'Archive')))), t('Cancel'));
  
}

/**
 * Submit callback for node_archive_multiple_archive_confirm()
 * 
 * Basically execute the archive operation and return to admin/content/node
 * 
 */
function node_archive_multiple_archive_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm'] && count($form_state['values']['nodes'])) {
    node_archive_mass_archive($form_state['values']['nodes']);
  }
  $form_state['redirect'] = 'admin/content/node';
  return;
}

/*****************************************************************
 * 
 * 3. Regular Drupal API functions
 * 
 *****************************************************************/

/**
 * Implementation of hook_menu_alter().
 * 
 * Replace the callback of admin/content/node - we are taking it over!
 * 
 * Also disable the ability to delete revisions if specified in options
 * 
 */
function node_archive_menu_alter(&$items) {
  $items['admin/content/node']['page arguments'] = array('node_archive_admin_content');
  
  // remove the menu item that allows deletion of revisions if set
  if (variable_get('node_archive_prevent_revision_deletion', '')) {
    unset($items['node/%node/revisions/%/delete']);
  }
}

/**
 * Implementation of hook_form_alter().
 * 
 * 1. Edit our own 'node_archive_admin_content' form at (admin/content/node)
 *    to remove the "Delete" operation
 * 
 * 2. If option set, disable the "Create new Revision" checkbox
 *    on the node edit form
 * 
 * 3. `If node is already archived add the "Restore" button
 * 
 * 4. If node is normal replace the delete button with the "Archive" button
 * 
 * 5. Alter the 'node_delete_confirm' form to represent archiving.
 *    We alter this form instead of making a new one because we want to
 *    piggyback on the "delete <all or own> <content type>" permissions.
 * 
 */
function node_archive_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'node_archive_admin_content') {
    if (isset($form['admin']['options']['operation']['#options']['delete'])) {
      unset($form['admin']['options']['operation']['#options']['delete']);
    }
  }
  
  if ($form['#id'] == 'node-form') {
    if (variable_get('node_archive_enforce_revisioning', '')) {
      // add this validate function to ensure a revision is made for every edit.
      $form['revision_information']['revision']['#disabled'] = true;
    }
    
    // this value is not always updated so ensure if is up-to-date
    $form['#node']->node_archived = _node_archive_is_archived($form['#node']->nid);
    
    if ($form['#node']->node_archived) {
      unset($form['buttons']['delete']);
      $form['buttons']['restore'] = array(
        '#type' => 'submit',
        '#value' => ucwords(variable_get('node_archive_restore_syntax', 'Restore')),
        '#weight' => 15,
        '#submit' => array('node_form_submit', 'node_archive_restore_submit'),
      );
    }
    else {
      $form['buttons']['delete']['#value'] = ucwords(variable_get('node_archive_archive_syntax', 'Archive'));
    }
  }
  
  if ($form_id == 'node_delete_confirm') {
    $node = $form['#parameters'][2];
    
    // look for menu items for this node
    $result = db_query("SELECT link_title, mlid, menu_name
                        FROM {menu_links}
                        WHERE link_path = 'node/%d'
                        AND module = 'menu'", $node->nid);
    
    $menu_items = array();
    while ($m = db_fetch_array($result)) {
      $menu_items[] = $m;
    }
    
    if (count($menu_items)) {
      $form['menu_items'] = array(
        '#tree' => TRUE,
        '#type' => 'fieldset',
        '#title' => t('Menu items'),
        '#weight' => -50,
        '#description' => t('One or more menu items are associated with this node. Please indicate below what you would like to do with them.'),
      );
      
      foreach ($menu_items as $menu_item) {
        $form['menu_items'][$menu_item['mlid']] = array(
          '#title' => '"' . $menu_item['link_title'] . '" in "' . $menu_item['menu_name'] . '"',
          '#type' => 'radios',
          '#default_value' => variable_get('node_archive_menu_link_archive', '2'),
          '#options' => array(
            1 => t('Delete this menu item.'),
            2 => t('Hide this menu item.'),
            0 => t('Leave this menu item as-is.')
          ),
          '#required' => TRUE,
        );
      }
    }
    
    // look for a path alias
    $result = db_query("SELECT pid, dst FROM {url_alias} WHERE src = 'node/%d'", $node->nid);
    $alias = db_fetch_array($result);
    
    if ($alias) {
      $form['alias'] = array(
        '#type' => 'fieldset',
        '#title' => t('URL Alias'),
        '#weight' => -40,
      );
      $form['alias']['delete_alias'] = array(
        '#type' => 'checkbox',
        '#title' => t('Delete the path alias: <strong>!alias</strong>',
          array('!alias' => $alias['dst'])),
        '#default_value' => variable_get('node_archive_path_alias_archive', '1'),
      );
    }
    
    
    unset($form['#submit']);
    $form['#submit'][] = 'node_archive_confirm_submit';
    $form['actions']['submit']['#value'] = t('Archive');
    $form['description']['#value'] = t('This action <strong>can</strong> be undone.');
    drupal_set_title(t('Are you sure you want to archive <em>%node</em>?',
      array('%node' => $node->title)));
  }
}

/**
 * Implementation of template_preprocess_page().
 * 
 * Add a body class to indicate the node is deleted.
 * 
 */
function node_archive_preprocess_page(&$vars) {
  $node = $vars['node'];
  if ($node->node_archived) {
    $vars['body_classes'] .= ' node-archived';
  }
}

/**
 * Implementation of hook_node_access_records().
 * 
 * Declare our module to the node_access system
 * 
 */
function node_archive_node_access_records($node) {
  // We only care about the node if it's been marked private. If not, it is
  // treated just like any other node and we completely ignore it.
  if ($node->node_archived) {
    $grants = array();
    $grants[] = array(
      'realm' => 'node_archive',
      'gid' => TRUE,
      'grant_view' => TRUE,
      'grant_update' => FALSE,
      'grant_delete' => FALSE,
      'priority' => 999,
    );
    return $grants;
  }
}

/**
 * Implementation of hook_node_grants().
 * 
 * Determine access to our archived nodes
 * 
 */
function node_archive_node_grants($account, $op) {
  if (($op == 'view' || $op == 'update' || $op == 'delete') && user_access('view archived nodes', $account)) {
    $grants['node_archive'] = array(1);
  }
  
  return $grants;
}

/**
 * Implementation of hook_node_operations().
 * 
 * Declare our node operations.
 * 
 */
function node_archive_node_operations() {
  $operations = array(
    'restore' => array(
      'label' => ucwords(variable_get('node_archive_restore_syntax', 'Restore')),
      'callback' => 'node_archive_mass_restore',
    ),
    'restore_publish' => array(
      'label' => t('!restore and Publish', array('!restore' => ucwords(variable_get('node_archive_restore_syntax', 'Restore')))), 
      'callback' => 'node_archive_mass_restore',
      'callback arguments' => array(1, 1), 
    ),
    'archive' => array(
      'label' => ucwords(variable_get('node_archive_archive_syntax', 'Archive')),
      'callback' => NULL, 
    ),
    'archive-vbo' => array(
      'label' => ucwords(variable_get('node_archive_archive_syntax', 'Archive')),
      'callback' => 'node_archive_mass_archive', 
    ),
  );
  return $operations;
}

/**
 * Implementation of hook_nodeapi().
 * 
 * Determine if a node is archived.
 * 
 * Enforce revisioning if option is set.
 * 
 */
function node_archive_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  switch ($op) {
    case 'load':
      $result = db_query("SELECT nid FROM {node_archive} WHERE nid = %d", $node->nid);
      if (db_result($result)) {
        $node->node_archived = 1;
      }
      else {
        $node->node_archived = 0;
      }
      break;
      
    case 'presave':
      // enforce revisioning if option set
      if (variable_get('node_archive_enforce_revisioning', '')) {
        $node->revision = TRUE;
      }
      break;
      
    case 'view':
    case 'prepare':
      if ($node->node_archived) {
        drupal_set_message(t('This node is currently !archived and will not be viewable by normal users. You can !restore it !here.', array('!archived' => strtolower(variable_get('node_archive_archived_syntax', 'Archived')), '!restore' => strtolower(variable_get('node_archive_restore_syntax', 'Restore')), '!here' => l(t('here'), 'node/' . $node->nid . '/restore'))));
      }
      break;
  }
}

/**
 * Implementation of hook_perm().
 * 
 * Declare our permissions.
 */
function node_archive_perm() {
  return array(
    'view archived nodes',
    'restore archived nodes',
  );
}

/**
 * Implementation of hook_menu().
 * 
 * Setup our pages
 */
function node_archive_menu() {
  $items['admin/settings/node-archive'] = array(
    'title' => 'Node Archive Settings', 
    'access arguments' => array('administer site configuration'),
    'file' => 'node_archive.admin.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('node_archive_settings'),
    'type' => MENU_NORMAL_ITEM,
  );
  
  $items['node/%node/restore'] = array(
    'title' => 'Restore',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('node_archive_restore_confirm', 1),
    'access arguments' => array('restore archived nodes'),
    'weight' => 1,
    'type' => MENU_CALLBACK);
  
  return $items;
}

/**
 * Implementation of hook_views_api().
 * 
 * Declare our views support
 */
function node_archive_views_api() {
  return array(
    'api' => 2,
    'path' => drupal_get_path('module', 'node_archive') .'/includes/views',
  );
}

/*****************************************************************
 * 
 * 3. Our own custom callbacks
 * (Not replacing an existing submit function)
 * 
 *****************************************************************/

/**
 * Submit handler for the "Restore" button.
 * 
 * Added into the node edit form
 * Send them to the 'node/<nid>/restore' page where we present a confirm form.
 * 
 */
function node_archive_restore_submit(&$form, &$form_state) {
//  if ($form_state['clicked_button']['#parents'][0] == 'restore') {
//    $form['#node']->node_archived = 0;
//    node_archive_mass_restore(array($form_state['values']['nid']));
//    $form_state['redirect'] = 'node/'. $form_state['values']['nid'];
//  }
  $destination = '';
  if (isset($_REQUEST['destination'])) {
    $destination = drupal_get_destination();
    unset($_REQUEST['destination']);
  }
  $node = $form['#node'];
  $form_state['redirect'] = array('node/'. $node->nid .'/restore', $destination);
}

/**
 * Menu callback -- ask for confirmation of node restoration
 */
function node_archive_restore_confirm(&$form_state, $node) {
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $node->nid,
  );
  $form['publish'] = array(
    '#type' => 'checkbox',
    '#title' => t('Publish as well.'),
    '#default_value' => 1,
  );
  
  // look for menu items for this node
  $result = db_query("SELECT link_title, mlid, menu_name
                      FROM {menu_links}
                      WHERE link_path = 'node/%d'
                      AND module = 'menu'
                      AND hidden = '-1'", $node->nid);

  $menu_items = array();
  while ($row = db_fetch_array($result)) {
    $menu_items[] = $row;
  }

  if (count($menu_items)) {
    $form['menu_items'] = array(
      '#tree' => TRUE,
      '#type' => 'fieldset',
      '#title' => t('Menu items'),
      '#weight' => -50,
      '#description' => t('One or more disabled menu items are associated with this node. Please indicate below what you would like to do with them.'),
    );

    foreach ($menu_items as $menu_item) {
      $form['menu_items'][$menu_item['mlid']] = array(
        '#title' => $menu_item['link_title'] .' in '. $menu_item['menu_name'],
        '#type' => 'radios',
        '#default_value' => variable_get('node_archive_menu_link_restore', '1'),
        '#options' => array(
          1 => t('Enable this menu item.'),
          0 => t('Leave this menu item disabled.')
        ),
        '#required' => TRUE,
      );
    }
  }

  return confirm_form($form,
    t('Are you sure you want to !restore %title?',
      array(
        '!restore' => strtolower(variable_get('node_archive_restore_syntax', 'Restore')),
        '%title' => $node->title)
      ),
    isset($_GET['destination']) ? $_GET['destination'] : 'node/'. $node->nid,
    t('This action <strong>can</strong> be undone.'),
    t('!restore', array('!restore' => ucwords(variable_get('node_archive_restore_syntax', 'Restore')))),
    t('Cancel')
  );
}

/**
 * Submit callback for node_archive_restore_confirm()
 */
function node_archive_restore_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $form['#node']->node_archived = 0;
    node_archive_mass_restore(
      array($form_state['values']['nid']),
      FALSE, $form_state['values']['publish'],
      $form_state['values']['menu_items']);
  }
  $form_state['redirect'] = 'node/'. $form_state['values']['nid'];
}