<?php

/**
 * @file
 * Admin settings for node_archive.
 */

function node_archive_settings() {
  $form = array();
  
  $form['syntax'] = array(
    '#type' => 'fieldset',
    '#title' => t('Syntax'),
    '#description' => t('Wording (syntax) of node states and actions. The terms "archive" and "restore" are used by default and will still appear in administration areas such as views.'),
  );
  
  $form['syntax']['node_archive_archive_syntax'] = array(
    '#type' => 'textfield',
    '#title' => t('What to call the action of archiving a node instead of "Archive"'),
    '#default_value' => variable_get('node_archive_archive_syntax', 'Archive'),
    '#required' => TRUE,
  );
  
  $form['syntax']['node_archive_archived_syntax'] = array(
    '#type' => 'textfield',
    '#title' => t('What to call an archive node instead of "Archived"'),
    '#default_value' => variable_get('node_archive_archived_syntax', 'Archived'),
    '#required' => TRUE,
  );
  
  $form['syntax']['node_archive_restore_syntax'] = array(
    '#type' => 'textfield',
    '#title' => t('What to call the action of restoring a node instead of "Restore"'),
    '#default_value' => variable_get('node_archive_restore_syntax', 'Restore'),
    '#required' => TRUE,
  );
  
  $form['syntax']['node_archive_restored_syntax'] = array(
    '#type' => 'textfield',
    '#title' => t('What to call the status of a node instead of "Restored"'),
    '#default_value' => variable_get('node_archive_restored_syntax', 'Restored'),
    '#required' => TRUE,
  );
  
  $form['revisioning'] = array(
    '#type' => 'fieldset',
    '#title' => t('Revisioning options'),
  );
  
  $form['revisioning']['node_archive_enforce_revisioning'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enforce revisioning on all nodes'),
    '#description' => t('This will force a revision to be created on every node edit.'),
    '#default_value' => variable_get('node_archive_enforce_revisioning', ''),
    '#required' => TRUE,
  );
  
  $form['revisioning']['node_archive_prevent_revision_deletion'] = array(
    '#type' => 'checkbox',
    '#title' => t('Prevent the deletion of node revisions'),
    '#description' => t("This will alter the menu callback so even admins won't be able to delete revisions."),
    '#default_value' => variable_get('node_archive_prevent_revision_deletion', ''),
    '#required' => TRUE,
  );
  
  $form['bulk'] = array(
    '#type' => 'fieldset',
    '#title' => t('Bulk options'),
    '#description' => t('The below options can be set per node while archiving or restoring. Bulk operations will use these settings by default.'),
  );
  
  $form['bulk']['node_archive_menu_link_archive'] = array(
    '#type' => 'radios',
    '#title' => t('Menu items when archiving a node'),
    '#description' => t("What should be done with the menu links when archiving a node?"),
    '#default_value' => variable_get('node_archive_menu_link_archive', '2'),
    '#options' => array(
      1 => t('Delete any existing menu links.'),
      2 => t('Hide any existing menu links.'),
      0 => t('Leave menu links as-is.')
    ),
    '#required' => TRUE,
  );
  
  $form['bulk']['node_archive_path_alias_archive'] = array(
    '#type' => 'checkbox',
    '#title' => t('Path alias when archiving a node'),
    '#description' => t("Delete existing path alias when archiving a node."),
    '#default_value' => variable_get('node_archive_path_alias_archive', '1'),
    '#required' => TRUE,
  );
  
  $form['bulk']['node_archive_menu_link_restore'] = array(
    '#type' => 'radios',
    '#title' => t('Menu items when restoring a node'),
    '#description' => t("What should be done with the disabled menu links when restoring a node?"),
    '#default_value' => variable_get('node_archive_menu_link_restore', '1'),
    '#options' => array(
      1 => t('Enable the menu links.'),
      0 => t('Leave menu links disabled.')
    ),
    '#required' => TRUE,
  );
  
  $form = system_settings_form($form);
  
  return $form;
}