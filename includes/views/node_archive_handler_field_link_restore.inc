<?php
/**
 * Field handler to present a link to delete a node.
 */
class node_archive_handler_field_link_restore extends views_handler_field_node_link {

  function render($values) {
    // ensure user has permission to restore this node.
    if (!user_access('restore archived nodes')) {
      return;
    }
    
    $text = !empty($this->options['text']) ? $this->options['text'] : ucwords(variable_get('node_archive_restore_syntax', 'Restore'));
    return l($text, "node/$values->nid/restore", array('query' => drupal_get_destination()));
  }
}

