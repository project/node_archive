<?php

class node_archive_handler_field_archived extends views_handler_field {
  function init(&$view, &$options) {
    parent::init($view, $options);

  }

  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }


  function render($values) {
    if ($values->{$this->field_alias}) {
      return 'Yes';
    }
    return 'No';
  }
}
